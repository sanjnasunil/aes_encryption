#!/usr/bin/env python3

#from Crypto.Cipher import AES
import binascii
import argparse
from encryption_steps import encrypt_chunk
from encryption_steps import decrypt_chunk


parser = argparse.ArgumentParser(description='Test AES implem')
parser.add_argument('integers', metavar='aes_size', type=int,
                    help='Which AES mode (128/256)')
parser.add_argument('cryptdef', metavar='aes_cryptdef', type=str,
                    help='Which crypt mode (encrypt/decrypt)')

args = parser.parse_args()
if not(args.integers == 128 or args.integers == 256):
    print("Incorrect AES size")
    exit()
elif args.integers == 128:
    keysize = 16
    num_rounds = 10
else:
    keysize = 32
    num_rounds = 14

if not(args.cryptdef == 'encrypt' or args.cryptdef == 'decrypt'):
    print("Incorrect AES cryptdef")
    exit()
elif args.cryptdef == 'encrypt':
    mode = 1

else:
    mode = 2

# Open the key file and convert it to an array of bytes
keyfile = open("key", "rb")
keyfile_contents = keyfile.read()
keyfile_bytes = bytearray(keyfile_contents)
key_array = [None] * keysize

for i in range(0, keysize):
    key_array[i] = ((hex(keyfile_bytes[i])[2:]))
    if len(key_array[i]) == 1:
        key_array[i] = "0" + key_array[i]

# Open the input file and convert it to a string of bytes in hex
# Convert the string to an array of bytes

#inputfile_bytes = binascii.hexlify(inputfile_contents.encode('utf8'))


if mode == 1:
    inputfile = open("input", "rb")
    #inputfile_contents = inputfile.read().splitlines()
    inputfile_contents = inputfile.read()
    inputfile_bytes = bytearray(inputfile_contents)
    #inputfile_bytes = bytearray(inputfile_contents[0])
    length = (16 - (len(inputfile_bytes) % 16)) % 16

    for x in range(0, length):
        inputfile_bytes += '\0'

    input_array = [None] * (len(inputfile_bytes))

    for i in range(0, (len(inputfile_bytes))):
        input_array[i] = ((hex(inputfile_bytes[i])[2:]))
        if len(input_array[i]) == 1:
            input_array[i] = "0" + input_array[i]

else:
    inputfile = open("input", "rb")
    inputfile_contents = inputfile.read().splitlines()
    inputfile_bytes = inputfile_contents[0].replace(" ", "")
    input_array = [None] * (len(inputfile_bytes) / 2)
    count = 0
    for i in range(0, (len(inputfile_bytes)), 2):
        input_array[count] = inputfile_bytes[i:i + 2]
        count += 1


# Slice the input bytes into 16 or 32 byte chunks, encrypt one chunk at a
# time and append to final result


num_chunks = len(input_array) / 16
start_position = 0
end_position = 16
output = []

for i in range(1, num_chunks + 1):

    current_chunk = input_array[start_position:end_position]
    if mode == 1:
        chunk = encrypt_chunk(current_chunk, key_array, keysize, num_rounds)
        output.extend(chunk)

    else:
        chunk = decrypt_chunk(current_chunk, key_array, keysize, num_rounds)
        output.extend(chunk)

    start_position += 16
    end_position += 16


output_string = ""
for x in output:
    output_string += x

output_file = open('outputfile', "wb")
output_file.write(output_string)
